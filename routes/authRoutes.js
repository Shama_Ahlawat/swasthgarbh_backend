const express = require('express')
const fetch = require('node-fetch')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')
const { jwtkey } = require('../keys')
const router = express.Router();
const User = mongoose.model('User');
const Doctor = mongoose.model('Doctor');
const Anc = mongoose.model('Anc');
const Patientadddata = mongoose.model('Patientadddata');
const Patient_regby_doc = mongoose.model('Patient_regby_doc');
const Patientmed = mongoose.model('Patientmed');
const Patientadddatabydoc = mongoose.model('Patientadddatabydoc');
const Adddatanotification = mongoose.model('Adddatanotification');
const Trimesternotification = mongoose.model('Trimesternotification');


router.use(bodyParser.json())
var jsonParser = bodyParser.json();

router.post('/patient_sendnotification', (req, res) => {
    var notification = {
        'title': 'title of notification',
        'text': 'body of notification'
    };
    var fcm_tokens = [];


    var notification_body = {
        'notification': notification,
        'registration_ids': fcm_tokens
    }

    fetch('http://fcm.googleapis.com/fcm/send', {
        'method': 'POST',
        'header': {
            'Authorization': 'key=' + 'AAAADcJIqm8:APA91bF3_T0aAMCNHnSc9Qj8rorYvvFhrMwK8EgdZ2GnaDrQcNrNHxbR06RBEOdUDx_GyG2toTXWxPKbcUv7mWJVaokM1alV6nPTg-XgQjpHvznwHIzJmgYcefnnHO_wwDR4TnAOzA2n',
            'Content-Type': 'application/json'
        },
        'body': JSON.stringify(notification_body)
    }).then(() => {
        res.status(200).send("notification send succesfully")
    }).catch((err) => {
        res.status(400).send("something went wrong")
        console.log(err);
    })
})

// patient creadentials signup and signin 

router.post('/patient_signup', async (req, res) => {
    console.log("api hiting")
    const { mobile, password, name,
        address, email, age,
        high_bp_check, history_pre, mother_sis,
        obesity, baby, any_history, sestatus, education, lmpdate, mobileDoctor, docname } = req.body;
    try {
        const user = new User({
            mobile, password, name, address, email, age, high_bp_check, history_pre, mother_sis, obesity,
            baby, any_history, sestatus, education, lmpdate, mobileDoctor, docname
        });
        await user.save();
        // res.send(user._id)
        // const token = jwt.sign ({userId:user._id},jwtkey)
        // res.send({token})
        jwt.sign({ userId: user }, "secretkey", (err, token) => {
            console.log(user)
            res.send({ token })
        });
    } catch (err) {
        res.status(422).send(err.message)
    }
})

router.post('/patient_signin', async (req, res) => {
    const { mobile, password } = req.body
    console.log("working")
    if (!mobile || !password) {
        return res.status(422).send({ error: "must provide correct mobile or password, ohaaa (ignore if u r doc)" })
    }
    const user = await User.findOne({ mobile })
    if (!user) {
        return res.status(422).send({ error: "must provide correct mobile or password, ulluu (ignore if u r doc)" })
    }
    try {
        await user.comparePassword(password);
        jwt.sign({ userId: user }, "secretkey", (err, token) => {
            res.send({ token })
            console.log(user.name)
        });
        // jwt.sign({user:user}, 'secretkey', (err, token)=>{
        //     res.send({token})
        // });
        // const token = jwt.sign ({userId:user._id},jwtkey)
        // res.send({token})
    } catch (err) {
        return res.status(422).send({ error: "must provide correct mobile or password. oongooo, (ignore if u r doc)" })
    }
})


/////doctor credentials signup aand signin

router.post('/doctor_signup', async (req, res) => {
    const { mobile, password, name,
        hospital, email, speciality } = req.body;
    try {
        const doctor = new Doctor({
            mobile, password, name,
            hospital, email, speciality
        });
        await doctor.save();
        // res.send(doctor._id)
        // const token = jwt.sign ({userId:user._id},jwtkey)
        // res.send({token})
        jwt.sign({ doctorId: doctor }, "secretkey", (err, token) => {
            res.send({ token })
            console.log(doctor)
            res.send(doctor.hospital)
        });
    } catch (err) {
        res.status(422).send(err.message)
    }
})

router.post('/doctor_signin', async (req, res) => {
    const { mobile, password } = req.body
    if (!mobile || !password) {
        return res.status(422).send({ error: "must provide correct mobile or password, ooppss, (ignore if u r patient)" })
    }
    const doctor = await Doctor.findOne({ mobile })
    if (!doctor) {
        return res.status(422).send({ error: "must provide correct mobile or password, blaaaa, (ignore if u r patient)" })
    }
    try {
        doctor.comparePassword(password);

        jwt.sign({ doctorId: doctor }, "secretkey", (err, token) => {
            res.send({ token })
            res.send(doctor.name)
        });
        // jwt.sign({user:user}, 'secretkey', (err, token)=>{
        //     res.send({token})
        // });
        // const token = jwt.sign ({userId:user._id},jwtkey)
        // res.send({token})
    } catch (err) {
        return res.status(422).send({ error: "must provide correct mobile or password, yoooo (ignore if u r patient)" })
    }
})
////// doctor work end

////////////// post add data



router.post('/patient_adddata', verifyToken, async (req, res) => {
    jwt.verify(req.token, "secretkey", async (err, authData) => {
        if (err) {
            return res.sendStatus(403)
        } else {
            console.log(authData)
            const { sys_BP, dys_BP, bodyweight, headache, date,
                abdominal, visual, fetal_movement, swelling, urine, bleed, heart_rate, extracomments, pat_mob } = req.body;
            const patientadddata = new Patientadddata({
                sys_BP, dys_BP, bodyweight, headache,
                abdominal, visual, fetal_movement, swelling, urine, bleed, heart_rate, extracomments, date, pat_mob
            });
            await patientadddata.save();
            res.send(patientadddata)
        }
    });
})

//// post add notification of add data

router.post('/addnotification_adddata', async (req, res) => {
    console.log("notification going to add")
    const { notification, date, pat_name, pat_mobile, doc_name, doc_mobile } = req.body;
    const adddatanotification = new Adddatanotification({
        notification, date, pat_name, pat_mobile, doc_name, doc_mobile
    });
    await adddatanotification.save();
    res.send(adddatanotification)
})

router.post('/trim_notification', async (req, res) => {
    console.log("sending trimester advice")
    const { first_trim, second_trim, third_trim, date, doc_mobile } = req.body;
    const trimesternotification = new Trimesternotification({
        first_trim, second_trim, third_trim, date, doc_mobile
    });
    await trimesternotification.save();
    res.send(trimesternotification)
})


//////post data of patient registered by doc /////////////////////
router.post('/patientuhid_adddata', async (req, res) => {
    console.log("hit, adding data")
    const {
        diabetes2,
        liver,
        kidney,
        apla,
        hyper,
        sle,
        mobile_pat,
        doc_mobile,
        native,
        postvalve,
        acy,
        cyn,
        ft_text,
        anc_text,
        mod_text,
        babyout_text,
        his_other,
        drug,
        anc1,
        anc1date,
        anc1_pog,
        anc1_fever,
        anc1_rash,
        anc1_vomit,
        anc1_bleed,
        anc1_pain,
        anc1_drug,
        anc1_smoke,
        anc1_alcohol,
        anc1_tob,
        anc1_caff,
        anc1_intimate,
        anc1his_other,
        anc1_pallor,
        anc1_ic,
        anc1_club,
        anc1_cyn,
        anc1_edema,
        anc1_lym,
        anc1_height,
        anc1_weight,
        anc1_bmi,
        anc1_PR,
        anc1_BP,
        anc1_RR,
        anc1_temp,
        anc1_chest,
        anc1_PA,
        anc1_prot,
        anc1_bloodgroup,
        anc1_hus_blood,
        anc1_bgICT,
        anc1_hiv,
        anc1_hbsag,
        anc1_vdrl,
        hiv_switch,
        hbsag_switch,
        vdrl_switch,
        anc1_urinerm,
        anc1_urinecs,
        anc1_hem,
        anc1_TSH,
        anc1_bloodfast,
        anc1_bloodpost,
        anc1_GTTfast,
        anc1_GTT1,
        anc1_GTT2,
        anc1_NTdone,
        anc1_NTCRL,
        anc1_NT,
        anc1_NTcen,
        anc1_NTtext,
        anc1_left,
        anc1_right,
        anc1_PIGF,
        anc1_PAPP,
        anc1_HCG,
        anc1_USGnormal,
        anc1_USGdone,
        anc1_investother,
        anc1_examother,
        anc1_adviceGTT,
        anc1_adviceblood,
        anc1_adviceNT,
        anc1_advicedual,
        anc1_advicefolate,
        anc1_adviceFeCa,
        anc1_adviceUSG,
        anc1_advicenut,
        anc1_advicenaus,
        anc1_adviceheart,
        anc1_adviceconst,
        anc1_advicepedal,
        anc1_adviceleg,
        anc1_adviceICT,
        anc1_advicediabetic,
        anc1_TSHnew,
        anc1_nitro,
        anc1_syp,
        anc1_Tvit,
        anc1_fluid,
        anc1_others,
        anc2_head,
        anc2_mict,
        anc2_per,
        anc2_pog,
        anc2_easy,
        anc2_short,
        anc2_spot,
        anc2date,
        anc2_pallor,
        anc2_pedal,
        anc2_PR,
        anc2_BP,
        anc2_weight,
        anc2_PA,
        anc2_hisother,
        anc2_examothers,
        anc2_investothers,
        anc2_adviceothers,
        anc2_quad,
        anc2_adviceGTT,
        anc2_adviceTFe,
        anc2_adviceTCa,
        anc2_adviceinj,
        anc2_advicequad,
        anc2_advicefetal,
        anc2_alb,
        anc2_Tfe,
        anc2_HPLC,
        anc2_peri,
        anc2_serum,
        anc2_nutadvice,
        anc2_advicenausea,
        anc2_adviceheart,
        anc2_adviceconst,
        anc2_advicepedal,
        anc2_adviceleg,
        anc2_advicediabetic,
        anc3_head,
        anc3_mict,
        anc3_short,
        anc3_easy,
        anc3_spot,
        anc3_leak,
        anc3_adequate,
        anc3_itching,
        anc3date,
        anc3_pog,
        anc3_pallor,
        anc3_pedal,
        anc3_PR,
        anc3_BP,
        anc3_weight,
        anc3_PA,
        anc3_fetalecho,
        anc3_hisother,
        anc3_examothers,
        anc3_adviceothers,
        anc3_investothers,
        anc3_adviceurine,
        anc3_adviceGTT,
        anc3_adviceICT,
        anc3_adviceTCa,
        anc3_adviceinj,
        anc3_advicelabour,
        anc3_adviceDFMC,
        anc3_inj,
        anc3_nutadvice,
        anc3_advicenausea,
        anc3_adviceheart,
        anc3_adviceconst,
        anc3_advicepedal,
        anc3_adviceleg,
        anc3_advicediabetic,
        anc4_head,
        anc4_mict,
        anc4_short,
        anc4_easy,
        anc4_spot,
        anc4_adequate,
        anc4_itching,
        anc4date,
        anc4_pog,
        anc4_pallor,
        anc4_pedal,
        anc4_PR,
        anc4_BP,
        anc4_weight,
        anc4_PA,
        anc4_culture,
        anc4_examICT,
        anc4_hisother,
        anc4_adviceothers,
        anc4_adviceTFe,
        anc4_adviceTCa,
        anc4_adviceUSG,
        anc4_adviceCBC,
        anc4_adviceKFT,
        anc4_adviceLFT,
        anc4_advicelabour,
        anc4_adviceDFMC,
        anc4_nutadvice,
        anc4_advicenausea,
        anc4_adviceheart,
        anc4_adviceconst,
        anc4_advicepedal,
        anc4_adviceleg,
        anc4_advicediabetic,
        anc5_head,
        anc5_mict,
        anc5_short,
        anc5_easy,
        anc5_spot,
        anc5_adequate,
        anc5_itching,
        anc5date,
        anc5_pog,
        anc5_pallor,
        anc5_pedal,
        anc5_PR,
        anc5_BP,
        anc5_weight,
        anc5_PA,
        anc5_HB,
        anc5_TLC,
        anc5_platelets,
        anc5_OT,
        anc5_PT,
        anc5_ALP,
        anc5_urea,
        anc5_Creat,
        anc5_vaginal,
        anc5_lscs,
        anc5_timing,
        anc5_adviceothers,
        anc5_hisother,
        anc5_atten,
        anc5_examothers,
        anc5_investcbc,
        anc5_investlft,
        anc5_investkft,
        anc5_investothers,
        anc5_HCweeks,
        anc5_HCcm,
        anc5_HCcen,
        anc5_usgBPDcm,
        anc5_usgBPDweeks,
        anc5_usgBPDcentile,
        anc5_ACweeks,
        anc5_ACcm,
        anc5_ACcen,
        anc5_FLweeks,
        anc5_FLcm,
        anc5_FLcen,
        anc5_EFWweeks,
        anc5_EFWgm,
        anc5_EFWcen,
        anc5_liquorafi,
        anc5_liquorslp,
        anc5_UAPI,
        anc5_UAPIcen,
        anc5_MCAPI,
        anc5_MCAPIcen,
        anc5_CPR,
        anc5_adviceTFe,
        anc5_adviceman,
        anc5_advicelabour,
        anc5_adviceDFMC,
        anc5_nutadvice,
        anc5_advicenausea,
        anc5_adviceheart,
        anc5_adviceconst,
        anc5_advicepedal,
        anc5_adviceleg,
        anc5_advicediabetic,
        anc6_head,
        anc6_mict,
        anc6_short,
        anc6_easy,
        anc6_spot,
        anc6_adequate,
        anc6_itching,
        anc6date,
        anc6_pog,
        anc6_pallor,
        anc6_pedal,
        anc6_PR,
        anc6_BP,
        anc6_weight,
        anc6_PA,
        anc6_adviceothers,
        anc6_hisother,
        anc6_pelvic,
        anc6_examothers,
        anc6_adviceTFe,
        anc6_adviceman,
        anc6_advicelabour,
        anc6_adviceDFMC,
        anc6_nutadvice,
        anc6_advicenausea,
        anc6_adviceheart,
        anc6_adviceconst,
        anc6_advicepedal,
        anc6_adviceleg,
        anc6_advicediabetic,
        anc7_head,
        anc7_mict,
        anc7_short,
        anc7_easy,
        anc7_spot,
        anc7_adequate,
        anc7_itching,
        anc7_pog,
        anc7_pallor,
        anc7_pedal,
        anc7_PR,
        anc7_BP,
        anc7_weight,
        anc7_adviceothers,
        anc7_adviceTFe,
        anc7_adviceman,
        anc7_advicelabour,
        anc7_adviceDFMC,
        anc7_advicenausea,
        anc7_nutadvice,
        anc7_adviceheart,
        anc7_adviceconst,
        anc7_advicepedal,
        anc7_adviceleg,
        anc7_advicediabetic,
        anc8_head,
        anc8_mict,
        anc8_short,
        anc8_easy,
        anc8_spot,
        anc8_adequate,
        anc8_itching,
        anc8_pog,
        anc8_pallor,
        anc8_pedal,
        anc8_PR,
        anc8_BP,
        anc8_weight,
        anc8_PA,
        anc8_adviceothers,
        anc8_hisother,
        anc8_examothers,
        anc8_adviceTFe,
        anc8_adviceman,
        anc8_adviceDFMC,
        anc8_advicenausea,
        anc8_nutadvice,
        anc8_adviceheart,
        anc8_adviceconst,
        anc8_advicepedal,
        anc8_adviceleg,
        anc8_advicediabetic } = req.body;
    const patientadddatabydoc = new Patientadddatabydoc({
        diabetes2, liver, kidney, apla, hyper, sle, mobile_pat, doc_mobile, native,
        postvalve,
        acy,
        cyn,
        ft_text,
        anc_text,
        mod_text,
        babyout_text,
        his_other,
        drug,
        anc1,
        anc1date,
        anc1_pog,
        anc1_fever,
        anc1_rash,
        anc1_vomit,
        anc1_bleed,
        anc1_pain,
        anc1_drug,
        anc1_smoke,
        anc1_alcohol,
        anc1_tob,
        anc1_caff,
        anc1_intimate,
        anc1his_other,
        anc1_pallor,
        anc1_ic,
        anc1_club,
        anc1_cyn,
        anc1_edema,
        anc1_lym,
        anc1_height,
        anc1_weight,
        anc1_bmi,
        anc1_PR,
        anc1_BP,
        anc1_RR,
        anc1_temp,
        anc1_chest,
        anc1_PA,
        anc1_prot,
        anc1_bloodgroup,
        anc1_hus_blood,
        anc1_bgICT,
        anc1_hiv,
        anc1_hbsag,
        anc1_vdrl,
        hiv_switch,
        hbsag_switch,
        vdrl_switch,
        anc1_urinerm,
        anc1_urinecs,
        anc1_hem,
        anc1_TSH,
        anc1_bloodfast,
        anc1_bloodpost,
        anc1_GTTfast,
        anc1_GTT1,
        anc1_GTT2,
        anc1_NTdone,
        anc1_NTCRL,
        anc1_NT,
        anc1_NTcen,
        anc1_NTtext,
        anc1_left,
        anc1_right,
        anc1_PIGF,
        anc1_PAPP,
        anc1_HCG,
        anc1_USGnormal,
        anc1_USGdone,
        anc1_investother,
        anc1_examother,
        anc1_adviceGTT,
        anc1_adviceblood,
        anc1_adviceNT,
        anc1_advicedual,
        anc1_advicefolate,
        anc1_adviceFeCa,
        anc1_adviceUSG,
        anc1_advicenut,
        anc1_advicenaus,
        anc1_adviceheart,
        anc1_adviceconst,
        anc1_advicepedal,
        anc1_adviceleg,
        anc1_adviceICT,
        anc1_advicediabetic,
        anc1_TSHnew,
        anc1_nitro,
        anc1_syp,
        anc1_Tvit,
        anc1_fluid,
        anc1_others,
        anc2_head,
        anc2_mict,
        anc2_per,
        anc2_pog,
        anc2_easy,
        anc2_short,
        anc2_spot,
        anc2date,
        anc2_pallor,
        anc2_pedal,
        anc2_PR,
        anc2_BP,
        anc2_weight,
        anc2_PA,
        anc2_hisother,
        anc2_examothers,
        anc2_investothers,
        anc2_adviceothers,
        anc2_quad,
        anc2_adviceGTT,
        anc2_adviceTFe,
        anc2_adviceTCa,
        anc2_adviceinj,
        anc2_advicequad,
        anc2_advicefetal,
        anc2_alb,
        anc2_Tfe,
        anc2_HPLC,
        anc2_peri,
        anc2_serum,
        anc2_nutadvice,
        anc2_advicenausea,
        anc2_adviceheart,
        anc2_adviceconst,
        anc2_advicepedal,
        anc2_adviceleg,
        anc2_advicediabetic,
        anc3_head,
        anc3_mict,
        anc3_short,
        anc3_easy,
        anc3_spot,
        anc3_leak,
        anc3_adequate,
        anc3_itching,
        anc3date,
        anc3_pog,
        anc3_pallor,
        anc3_pedal,
        anc3_PR,
        anc3_BP,
        anc3_weight,
        anc3_PA,
        anc3_fetalecho,
        anc3_hisother,
        anc3_examothers,
        anc3_adviceothers,
        anc3_investothers,
        anc3_adviceurine,
        anc3_adviceGTT,
        anc3_adviceICT,
        anc3_adviceTCa,
        anc3_adviceinj,
        anc3_advicelabour,
        anc3_adviceDFMC,
        anc3_inj,
        anc3_nutadvice,
        anc3_advicenausea,
        anc3_adviceheart,
        anc3_adviceconst,
        anc3_advicepedal,
        anc3_adviceleg,
        anc3_advicediabetic,
        anc4_head,
        anc4_mict,
        anc4_short,
        anc4_easy,
        anc4_spot,
        anc4_adequate,
        anc4_itching,
        anc4date,
        anc4_pog,
        anc4_pallor,
        anc4_pedal,
        anc4_PR,
        anc4_BP,
        anc4_weight,
        anc4_PA,
        anc4_culture,
        anc4_examICT,
        anc4_hisother,
        anc4_adviceothers,
        anc4_adviceTFe,
        anc4_adviceTCa,
        anc4_adviceUSG,
        anc4_adviceCBC,
        anc4_adviceKFT,
        anc4_adviceLFT,
        anc4_advicelabour,
        anc4_adviceDFMC,
        anc4_nutadvice,
        anc4_advicenausea,
        anc4_adviceheart,
        anc4_adviceconst,
        anc4_advicepedal,
        anc4_adviceleg,
        anc4_advicediabetic,
        anc5_head,
        anc5_mict,
        anc5_short,
        anc5_easy,
        anc5_spot,
        anc5_adequate,
        anc5_itching,
        anc5date,
        anc5_pog,
        anc5_pallor,
        anc5_pedal,
        anc5_PR,
        anc5_BP,
        anc5_weight,
        anc5_PA,
        anc5_HB,
        anc5_TLC,
        anc5_platelets,
        anc5_OT,
        anc5_PT,
        anc5_ALP,
        anc5_urea,
        anc5_Creat,
        anc5_vaginal,
        anc5_lscs,
        anc5_timing,
        anc5_adviceothers,
        anc5_hisother,
        anc5_atten,
        anc5_examothers,
        anc5_investcbc,
        anc5_investlft,
        anc5_investkft,
        anc5_investothers,
        anc5_HCweeks,
        anc5_HCcm,
        anc5_HCcen,
        anc5_usgBPDcm,
        anc5_usgBPDweeks,
        anc5_usgBPDcentile,
        anc5_ACweeks,
        anc5_ACcm,
        anc5_ACcen,
        anc5_FLweeks,
        anc5_FLcm,
        anc5_FLcen,
        anc5_EFWweeks,
        anc5_EFWgm,
        anc5_EFWcen,
        anc5_liquorafi,
        anc5_liquorslp,
        anc5_UAPI,
        anc5_UAPIcen,
        anc5_MCAPI,
        anc5_MCAPIcen,
        anc5_CPR,
        anc5_adviceTFe,
        anc5_adviceman,
        anc5_advicelabour,
        anc5_adviceDFMC,
        anc5_nutadvice,
        anc5_advicenausea,
        anc5_adviceheart,
        anc5_adviceconst,
        anc5_advicepedal,
        anc5_adviceleg,
        anc5_advicediabetic,
        anc6_head,
        anc6_mict,
        anc6_short,
        anc6_easy,
        anc6_spot,
        anc6_adequate,
        anc6_itching,
        anc6date,
        anc6_pog,
        anc6_pallor,
        anc6_pedal,
        anc6_PR,
        anc6_BP,
        anc6_weight,
        anc6_PA,
        anc6_adviceothers,
        anc6_hisother,
        anc6_pelvic,
        anc6_examothers,
        anc6_adviceTFe,
        anc6_adviceman,
        anc6_advicelabour,
        anc6_adviceDFMC,
        anc6_nutadvice,
        anc6_advicenausea,
        anc6_adviceheart,
        anc6_adviceconst,
        anc6_advicepedal,
        anc6_adviceleg,
        anc6_advicediabetic,
        anc7_head,
        anc7_mict,
        anc7_short,
        anc7_easy,
        anc7_spot,
        anc7_adequate,
        anc7_itching,
        anc7_pog,
        anc7_pallor,
        anc7_pedal,
        anc7_PR,
        anc7_BP,
        anc7_weight,
        anc7_adviceothers,
        anc7_adviceTFe,
        anc7_adviceman,
        anc7_advicelabour,
        anc7_adviceDFMC,
        anc7_advicenausea,
        anc7_nutadvice,
        anc7_adviceheart,
        anc7_adviceconst,
        anc7_advicepedal,
        anc7_adviceleg,
        anc7_advicediabetic,
        anc8_head,
        anc8_mict,
        anc8_short,
        anc8_easy,
        anc8_spot,
        anc8_adequate,
        anc8_itching,
        anc8_pog,
        anc8_pallor,
        anc8_pedal,
        anc8_PR,
        anc8_BP,
        anc8_weight,
        anc8_PA,
        anc8_adviceothers,
        anc8_hisother,
        anc8_examothers,
        anc8_adviceTFe,
        anc8_adviceman,
        anc8_adviceDFMC,
        anc8_advicenausea,
        anc8_nutadvice,
        anc8_adviceheart,
        anc8_adviceconst,
        anc8_advicepedal,
        anc8_adviceleg,
        anc8_advicediabetic
    });
    await patientadddatabydoc.save();
    res.send(patientadddatabydoc)
});


router.post('/anc_data', verifyToken, async (req, res) => {
    jwt.verify(req.token, "secretkey", async (err, authData) => {
        if (err) {
            return res.sendStatus(403)
        } else {
            console.log(authData)
            const { anc1_date,
                anc2_date,
                anc3_date,
                anc4_date,
                anc5_date,
                anc6_date,
                anc7_date,
                anc8_date,
                anc1diabetes,
                anc1anaemia,
                anc1ultrasound,
                anc1HIV,
                anc1tetnus,
                anc1urinetest,
                anc2diabetes,
                anc2ultrasound,
                anc3diabetes,
                anc3anaemia,
                anc3urinetest,
                anc4diabetes,
                anc5diabetes,
                anc5urinetest,
                anc6diabetes,
                anc6anaemia,
                anc7diabetes,
                anc8diabetes, mobile } = req.body;
            const ancdata = new Anc({
                anc1_date,
                anc2_date,
                anc3_date,
                anc4_date,
                anc5_date,
                anc6_date,
                anc7_date,
                anc8_date,
                anc1diabetes,
                anc1anaemia,
                anc1ultrasound,
                anc1HIV,
                anc1tetnus,
                anc1urinetest,
                anc2diabetes,
                anc2ultrasound,
                anc3diabetes,
                anc3anaemia,
                anc3urinetest,
                anc4diabetes,
                anc5diabetes,
                anc5urinetest,
                anc6diabetes,
                anc6anaemia,
                anc7diabetes,
                anc8diabetes, mobile
            });
            await ancdata.save();
            res.send(ancdata)
        }
    });
})

router.post('/patient_medicine', verifyToken, async (req, res) => {
    console.log(" patient_medicine api hiting")
    jwt.verify(req.token, "secretkey", async (err, authData) => {
        if (err) {
            return res.sendStatus(403)
        } else {
            // console.log(authData)
            const { data, checked, med_name, from_date, to_date, extra_med, sos_switch, mobile, doc_mob, dose_count, med_quantity, med_reason, schedule_type, selected_days, alarm_timer } = req.body;
            const patientmed = new Patientmed({
                data, checked, med_name, from_date, to_date, extra_med, sos_switch, mobile, doc_mob, dose_count, med_quantity, med_reason, schedule_type, selected_days, alarm_timer
            });
            await patientmed.save();
            res.send(patientmed)
        }
    });
})

/////////// uhid patient signup and signin
router.post('/patient_signupbydoc', async (req, res) => {
    const { mobile, password, name, docmob, docname,
        address, email, age,
        high_bp_check, history_pre, mother_sis,
        obesity, baby, any_history, sestatus, education, lmpdate, uhid } = req.body;
    try {
        const patient_regby_doc = new Patient_regby_doc({
            mobile, password, name, address, email, age, high_bp_check, history_pre, mother_sis, obesity,
            baby, any_history, sestatus, education, lmpdate, uhid, docmob, docname
        });
        await patient_regby_doc.save();


        jwt.sign({ patient_regby_docId: patient_regby_doc }, "secretkey", (err, token) => {
            console.log(patient_regby_doc)
            res.send({ token })
        });


    } catch (err) {
        res.status(422).send(err.message)
    }
})


router.post('/patient_signinbydoc', async (req, res) => {
    console.log("api hit")
    const { mobile, password } = req.body
    console.log("yes got it")
    if (!mobile || !password) {
        return res.status(422).send({ error: "must provide correct mobile or password, ohaaa (ignore if u r not reg by doc)" })
    }
    const patient_regby_doc = await Patient_regby_doc.findOne({ mobile })
    if (!patient_regby_doc) {
        return res.status(422).send({ error: "must provide correct mobile or password, ulluu (ignore if u r not reg by doc)" })
    }
    try {
        await patient_regby_doc.comparePassword(password);
        console.log(patient_regby_doc.lmpdate)
        jwt.sign({ patient_regby_docId: patient_regby_doc }, "secretkey", (err, token) => {
            res.send({ token })
        });
    } catch (err) {
        return res.status(422).send({ error: "must provide correct mobile or password. oongooo, (ignore if u r not reg by doc)" })
    }
})

// {expiresIn: '2min'}


router.get('/doctorlist', async (req, res) => {
    try {
        const doctors = await Doctor.find({})
        res.send(doctors)
    } catch (error) {
        res.sendStatus(error)
    }
})

router.delete('/doctor_delete', async (req, res) => {
    Doctor.deleteOne({ mobile: req.query.mobile }).then(
        () => {
            res.status(200).json({
                message: 'Deleted!'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
})

router.get('/patientlist', async (req, res) => {
    console.log("patientlist", req.params.id);
    try {
        const patients = await User.find({})
        res.send(patients)
    } catch (error) {
        res.sendStatus(error)
    }
})

router.get('/patientwithuhidlist', async (req, res) => {
    try {
        const patientswithuhid = await Patient_regby_doc.find({})
        res.send(patientswithuhid)
        console.log(patientswithuhid)
    } catch (error) {
        res.sendStatus(error)
    }
})

router.get('/patientadddata_list', async (req, res) => {
    try {
        const patientadddata = await Patientadddata.find({})
        res.send(patientadddata)
    } catch (error) {
        res.sendStatus(error)
    }
})

router.get('/patientnotification_list', async (req, res) => {
    try {
        const adddatanotification = await Adddatanotification.find({})
        res.send(adddatanotification)
    } catch (error) {
        res.sendStatus(error)
    }
})

router.get('/patientmedicine_list', async (req, res) => {
    try {
        const patientmed = await Patientmed.find({})
        res.send(patientmed)
    } catch (error) {
        res.sendStatus(error)
    }
})

router.get('/patientlist/:id', async (req, res) => {
    try {
        console.log("hello", req.params.id);
        const particular_user = await User.findById(req.params.id)
        res.send(particular_user)
    } catch (error) {
        res.sendStatus(error)
    }
})

router.get('/medicinelist_by', async (req, res) => {
    try {
        console.log("medicinelist_by", req.query);
        let query = {};
        if (req.query.doc_mob) {
            query["doc_mob"] = req.query.doc_mob;
        }
        if (req.query.mobile) {
            query["mobile"] = req.query.mobile
        }
        console.log("query::", query);
        const schedule_medicine = await Patientmed.find(query)
        res.send(schedule_medicine)
    } catch (error) {
        res.sendStatus(error)
    }
})

router.delete('/delete_medicine', async (req, res) => {
    Patientmed.deleteOne({ _id: req.query.id }).then(
        () => {
            res.status(200).json({
                message: 'Deleted!'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
})
/////////////////////////update anc data/////////////////
router.put('/anc_data/:id', verifyToken, jsonParser, function (req, res) {
    jwt.verify(req.token, "secretkey", async (err, authData) => {
        if (err) {
            return res.sendStatus(403)
        } else {
            console.log("hello", req.params.id);
            Anc.update({ _id: req.params.id },
                {
                    $set: {
                        anc1diabetes: req.body.anc1diabetes,
                        anc1anaemia: req.body.anc1anaemia,
                        anc1ultrasound: req.body.anc1ultrasound,
                        anc1HIV: req.body.anc1HIV,
                        anc1tetnus: req.body.anc1tetnus,
                        anc1urinetest: req.body.anc1urinetest,
                        anc2diabetes: req.body.anc2diabetes,
                        anc2ultrasound: req.body.anc2ultrasound,
                        anc3diabetes: req.body.anc3diabetes,
                        anc3anaemia: req.body.anc3anaemia,
                        anc3urinetest: req.body.anc3urinetest,
                        anc4diabetes: req.body.anc4diabetes,
                        anc5diabetes: req.body.anc5diabetes,
                        anc5urinetest: req.body.anc5urinetest,
                        anc6diabetes: req.body.anc6diabetes,
                        anc6anaemia: req.body.anc6anaemia,
                        anc7diabetes: req.body.anc7diabetes,
                        anc8diabetes: req.body.anc8diabetes,
                        mobile: req.body.mobile
                    }
                }
            ).then((result) => {
                res.status(200).json(result)
            }).catch((err) => { console.warn(err) })
        }
    })
})

/////////////////////update uhid vale patient ka add data //////////
router.put('/patient_udatedata/:id', function (req, res) {
    console.log("koi bhi error nhi h")
    console.log("hello", req.params.id);
    Patientadddatabydoc.update({ _id: req.params.id },
        {
            $set: {
                diabetes2: req.body.diabetes2, liver: req.body.liver,
                kidney: req.body.kidney, apla: req.body.apla,
                hyper: req.body.hyper, sle: req.body.sle,
                native: req.body.native,
                postvalve: req.body.postvalve,
                acy: req.body.acy, cyn: req.body.cyn,
                ft_text: req.body.ft_text,
                anc_text: req.body.anc_text,
                mod_text: req.body.mod_text,
                babyout_text: req.body.babyout_text,
                his_other: req.body.his_other,
                drug: req.body.drug,
                anc1: req.body.anc1,
                anc1date: req.body.anc1date,
                anc1_pog: req.body.anc1_pog,
                anc1_fever: req.body.anc1_fever,
                anc1_rash: req.body.anc1_rash,
                anc1_vomit: req.body.anc1_vomit,
                anc1_bleed: req.body.anc1_bleed,
                anc1_pain: req.body.anc1_pain,
                anc1_drug: req.body.anc1_drug,
                anc1_smoke: req.body.anc1_smoke,
                anc1_alcohol: req.body.anc1_alcohol,
                anc1_tob: req.body.anc1_tob,
                anc1_caff: req.body.anc1_caff,
                anc1_intimate: req.body.anc1_intimate,
                anc1his_other: req.body.anc1his_other,
                anc1_pallor: req.body.anc1_pallor,
                anc1_ic: req.body.anc1_ic,
                anc1_club: req.body.anc1_club,
                anc1_cyn: req.body.anc1_cyn,
                anc1_edema: req.body.anc1_edema,
                anc1_lym: req.body.anc1_lym,
                anc1_height: req.body.anc1_height,
                anc1_weight: req.body.anc1_weight,
                anc1_bmi: req.body.anc1_bmi,
                anc1_PR: req.body.anc1_PR,
                anc1_BP: req.body.anc1_BP,
                anc1_PR: req.body.anc1_RR,
                anc1_temp: req.body.anc1_temp,
                anc1_chest: req.body.anc1_chest,
                anc1_PA: req.body.anc1_PA,
                anc1_prot: req.body.anc1_prot,
                anc1_bloodgroup: req.body.anc1_bloodgroup,
                anc1_hus_blood: req.body.anc1_hus_blood,
                anc1_bgICT: req.body.anc1_bgICT,
                anc1_hiv: req.body.anc1_hiv,
                anc1_hbsag: req.body.anc1_hbsag,
                anc1_vdrl: req.body.anc1_vdrl,
                hiv_switch: req.body.hiv_switch,
                hbsag_switch: req.body.hbsag_switch,
                vdrl_switch: req.body.vdrl_switch,
                anc1_urinerm: req.body.anc1_urinerm,
                anc1_urinecs: req.body.anc1_urinecs,
                anc1_hem: req.body.anc1_hem,
                anc1_TSH: req.body.anc1_TSH,
                anc1_bloodfast: req.body.anc1_bloodfast,
                anc1_bloodpost: req.body.anc1_bloodpost,
                anc1_GTTfast: req.body.anc1_GTTfast,
                anc1_GTT1: req.body.anc1_GTT1,
                anc1_GTT2: req.body.anc1_GTT2,
                aanc1_NTdone: req.body.anc1_NTdone,
                aanc1_NTCRL: req.body.anc1_NTCRL,
                anc1_NT: req.body.anc1_NT,
                anc1_NTcen: req.body.anc1_NTcen,
                anc1_NTtext: req.body.anc1_NTtext,
                anc1_left: req.body.anc1_left,
                anc1_right: req.body.anc1_right,
                anc1_PIGF: req.body.anc1_PIGF,
                anc1_PAPP: req.body.anc1_PAPP,
                anc1_HCG: req.body.anc1_HCG,
                anc1_USGnormal: req.body.anc1_USGnormal,
                anc1_USGdone: req.body.anc1_USGdone,
                anc1_investother: req.body.anc1_investother,
                anc1_examother: req.body.anc1_examother,
                anc1_adviceGTT: req.body.anc1_adviceGTT,
                anc1_adviceblood: req.body.anc1_adviceblood,
                anc1_adviceNT: req.body.anc1_adviceNT,
                anc1_advicedual: req.body.anc1_advicedual,
                anc1_advicefolate: req.body.anc1_advicefolate,
                anc1_adviceFeCa: req.body.anc1_adviceFeCa,
                anc1_adviceUSG: req.body.anc1_adviceUSG,
                anc1_advicenut: req.body.anc1_advicenut,
                anc1_advicenaus: req.body.anc1_advicenaus,
                anc1_adviceheart: req.body.anc1_adviceheart,
                anc1_adviceconst: req.body.anc1_adviceconst,
                anc1_advicepedal: req.body.anc1_advicepedal,
                anc1_adviceleg: req.body.anc1_adviceleg,
                anc1_adviceICT: req.body.anc1_adviceICT,
                anc1_advicediabetic: req.body.anc1_advicediabetic,
                anc1_TSHnew: req.body.anc1_TSHnew,
                anc1_nitro: req.body.anc1_nitro,
                anc1_syp: req.body.anc1_syp,
                anc1_Tvit: req.body.anc1_Tvit,
                anc1_fluid: req.body.anc1_fluid,
                anc1_others: req.body.anc1_others,
                anc2_head: req.body.anc2_head,
                anc2_mict: req.body.anc2_mict,
                anc2_per: req.body.anc2_per,
                anc2_pog: req.body.anc2_pog,
                anc2_easy: req.body.anc2_easy,
                anc2_short: req.body.anc2_short,
                anc2_spot: req.body.anc2_spot,
                anc2date: req.body.anc2date,
                anc2_pallor: req.body.anc2_pallor,
                anc2_pedal: req.body.anc2_pedal,
                anc2_PR: req.body.anc2_PR,
                anc2_BP: req.body.anc2_BP,
                anc2_weight: req.body.anc2_weight,
                anc2_PA: req.body.anc2_PA,
                anc2_hisother: req.body.anc2_hisother,
                anc2_examothers: req.body.anc2_examothers,
                anc2_investothers: req.body.anc2_investothers,
                anc2_adviceothers: req.body.anc2_adviceothers,
                anc2_quad: req.body.anc2_quad,
                anc2_adviceGTT: req.body.anc2_adviceGTT,
                anc2_adviceTFe: req.body.anc2_adviceTFe,
                anc2_adviceTCa: req.body.anc2_adviceTCa,
                anc2_adviceinj: req.body.anc2_adviceinj,
                anc2_advicequad: req.body.anc2_advicequad,
                anc2_advicefetal: req.body.anc2_advicefetal,
                anc2_alb: req.body.anc2_alb,
                anc2_Tfe: req.body.anc2_Tfe,
                anc2_HPLC: req.body.anc2_HPLC,
                anc2_peri: req.body.anc2_peri,
                anc2_serum: req.body.anc2_serum,
                anc2_nutadvice: req.body.anc2_nutadvice,
                anc2_advicenausea: req.body.anc2_advicenausea,
                anc2_adviceheart: req.body.anc2_adviceheart,
                anc2_adviceconst: req.body.anc2_adviceconst,
                anc2_advicepedal: req.body.anc2_advicepedal,
                anc2_adviceled: req.body.anc2_adviceleg,
                anc2_advicediabetic: req.body.anc2_advicediabetic,
                anc3_head: req.body.anc3_head,
                anc3_mict: req.body.anc3_mict,
                anc3_short: req.body.anc3_short,
                anc3_easy: req.body.anc3_easy,
                anc3_spot: req.body.anc3_spot,
                anc3_leak: req.body.anc3_leak,
                anc3_adequate: req.body.anc3_adequate,
                anc3_itching: req.body.anc3_itching,
                anc3date: req.body.anc3date,
                anc3_pog: req.body.anc3_pog,
                anc3_pallor: req.body.anc3_pallor,
                anc3_pedal: req.body.anc3_pedal,
                anc3_PR: req.body.anc3_PR,
                anc3_BP: req.body.anc3_BP,
                anc33_weight: req.body.anc3_weight,
                anc3_PA: req.body.anc3_PA,
                anc3_fetalecho: req.body.anc3_fetalecho,
                anc3_hisother: req.body.anc3_hisother,
                anc3_examothers: req.body.anc3_examothers,
                anc3_adviceothers: req.body.anc3_adviceothers,
                anc3_inestothers: req.body.anc3_investothers,
                anc3_adviceurine: req.body.anc3_adviceurine,
                anc3_adviceGTT: req.body.anc3_adviceGTT,
                anc3_adviceICT: req.body.anc3_adviceICT,
                anc3_adviceTCa: req.body.anc3_adviceTCa,
                anc3_adviceinj: req.body.anc3_adviceinj,
                anc3_advicelabour: req.body.anc3_advicelabour,
                anc3_adviceDFMC: req.body.anc3_adviceDFMC,
                anc3_inj: req.body.anc3_inj,
                anc3_nutadvice: req.body.anc3_nutadvice,
                anc3_advicenausea: req.body.anc3_advicenausea,
                anc3_adviceheart: req.body.anc3_adviceheart,
                anc3_adviceconst: req.body.anc3_adviceconst,
                anc3_advicepedal: req.body.anc3_advicepedal,
                anc3_adviceleg: req.body.anc3_adviceleg,
                anc3_advicediabetic: req.body.anc3_advicediabetic,
                anc4_head: req.body.anc4_head,
                anc4_mict: req.body.anc4_mict,
                anc4_short: req.body.anc4_short,
                anc4_easy: req.body.anc4_easy,
                anc4_spot: req.body.anc4_spot,
                anc4_adequate: req.body.anc4_adequate,
                anc4_itching: req.body.anc4_itching,
                anc4date: req.body.anc4date,
                anc4_pog: req.body.anc4_pog,
                anc4_pallor: req.body.anc4_pallor,
                anc4_pedal: req.body.anc4_pedal,
                anc4_PR: req.body.anc4_PR,
                anc4_BP: req.body.anc4_BP,
                anc4_weight: req.body.anc4_weight,
                anc4_PA: req.body.anc4_PA,
                anc4_culture: req.body.anc4_culture,
                anc4_examICT: req.body.anc4_examICT,
                anc4_hisother: req.body.anc4_hisother,
                anc4_adviceothers: req.body.anc4_adviceothers,
                anc4_adviceTFe: req.body.anc4_adviceTFe,
                anc4_adviceTCa: req.body.anc4_adviceTCa,
                anc4_adviceUSG: req.body.anc4_adviceUSG,
                anc4_adviceCBC: req.body.anc4_adviceCBC,
                anc4_adviceKFT: req.body.anc4_adviceKFT,
                anc4_adviceLFT: req.body.anc4_adviceLFT,
                anc4_advicelabour: req.body.anc4_advicelabour,
                anc4_adviceDFMC: req.body.anc4_adviceDFMC,
                anc4_nutadvice: req.body.anc4_nutadvice,
                anc4_advicenausea: req.body.anc4_advicenausea,
                anc4_adviceheart: req.body.anc4_adviceheart,
                anc4_adviceconst: req.body.anc4_adviceconst,
                anc4_advicepedal: req.body.anc4_advicepedal,
                anc4_adviceleg: req.body.anc4_adviceleg,
                anc4_advicediabetic: req.body.anc4_advicediabetic,
                anc5_head: req.body.anc5_head,
                anc5_mict: req.body.anc5_mict,
                anc5_short: req.body.anc5_short,
                anc5_easy: req.body.anc5_easy,
                anc5_spot: req.body.anc5_spot,
                anc5_adequate: req.body.anc5_adequate,
                anc5_itching: req.body.anc5_itching,
                anc5date: req.body.anc5date,
                anc5_pog: req.body.anc5_pog,
                anc5_pallor: req.body.anc5_pallor,
                anc5_pedal: req.body.anc5_pedal,
                anc5_PR: req.body.anc5_PR,
                anc5_BP: req.body.anc5_BP,
                anc5_weight: req.body.anc5_weight,
                anc5_PA: req.body.anc5_PA,
                anc5_HB: req.body.anc5_HB,
                anc5_TLC: req.body.anc5_TLC,
                anc5_platelets: req.body.anc5_platelets,
                anc5_OT: req.body.anc5_OT,
                anc5_PT: req.body.anc5_PT,
                anc5_ALP: req.body.anc5_ALP,
                anc5_urea: req.body.anc5_urea,
                anc5_Creat: req.body.anc5_Creat,
                anc5_vaginal: req.body.anc5_vaginal,
                anc5_lscs: req.body.anc5_lscs,
                anc5_timing: req.body.anc5_timing,
                anc5_adviceothers: req.body.anc5_adviceothers,
                anc5_hisother: req.body.anc5_hisother,
                anc5_atten: req.body.anc5_atten,
                anc5_examothers: req.body.anc5_examothers,
                anc5_investcbc: req.body.anc5_investcbc,
                anc5_investlft: req.body.anc5_investlft,
                anc5_investkft: req.body.anc5_investkft,
                anc5_investothers: req.body.anc5_investothers,
                anc5_HCweeks: req.body.anc5_HCweeks,
                anc5_HCcm: req.body.anc5_HCcm,
                anc5_HCcen: req.body.anc5_HCcen,
                anc5_usgBPDcm: req.body.anc5_usgBPDcm,
                anc5_usgBPDweeks: req.body.anc5_usgBPDweeks,
                anc5_usgBPDcentile: req.body.anc5_usgBPDcentile,
                anc5_ACweeks: req.body.anc5_ACweeks,
                anc5_ACcm: req.body.anc5_ACcm,
                anc5_ACcen: req.body.anc5_ACcen,
                anc5_FLweeks: req.body.anc5_FLweeks,
                anc5_FLcm: req.body.anc5_FLcm,
                anc5_FLcen: req.body.anc5_FLcen,
                anc5_EFWweeks: req.body.anc5_EFWweeks,
                anc5_EFWgm: req.body.anc5_EFWgm,
                anc5_EFWcen: req.body.anc5_EFWcen,
                anc5_liquorafi: req.body.anc5_liquorafi,
                anc5_liqourslp: req.body.anc5_liquorslp,
                anc5_UAPI: req.body.anc5_UAPI,
                anc5_UAPIcen: req.body.anc5_UAPIcen,
                anc5_MCAPI: req.body.anc5_MCAPI,
                anc5_MCAPIcen: req.body.anc5_MCAPIcen,
                anc5_CPR: req.body.anc5_CPR,
                anc5_adviceTFe: req.body.anc5_adviceTFe,
                anc5_adviceman: req.body.anc5_adviceman,
                anc5_advicelabour: req.body.anc5_advicelabour,
                anc5_adviceDFMC: req.body.anc5_adviceDFMC,
                anc5_nutadvice: req.body.anc5_nutadvice,
                anc5_advicenausea: req.body.anc5_advicenausea,
                anc5_adviceheart: req.body.anc5_adviceheart,
                anc5_adviceconst: req.body.anc5_adviceconst,
                anc5_advicepedal: req.body.anc5_advicepedal,
                anc5_adviceleg: req.body.anc5_adviceleg,
                anc5_advicediabetic: req.body.anc5_advicediabetic,
                anc6_head: req.body.anc6_head,
                anc6_mict: req.body.anc6_mict,
                anc6_short: req.body.anc6_short,
                anc6_easy: req.body.anc6_easy,
                anc6_spot: req.body.anc6_spot,
                anc6_adequate: req.body.anc6_adequate,
                anc6_itching: req.body.anc6_itching,
                anc6date: req.body.anc6date,
                anc6_pog: req.body.anc6_pog,
                anc6_pallor: req.body.anc6_pallor,
                anc6_pedal: req.body.anc6_pedal,
                anc6_PR: req.body.anc6_PR,
                anc6_BP: req.body.anc6_BP,
                anc6_weight: req.body.anc6_weight,
                anc6_PA: req.body.anc6_PA,
                anc6_adviceothers: req.body.anc6_adviceothers,
                anc6_hisother: req.body.anc6_hisother,
                anc6_pelvic: req.body.anc6_pelvic,
                anc6_examothers: req.body.anc6_examothers,
                anc6_adviceTFe: req.body.anc6_adviceTFe,
                anc6_adviceman: req.body.anc6_adviceman,
                anc6_advicelabour: req.body.anc6_advicelabour,
                anc6_adviceDFMC: req.body.anc6_adviceDFMC,
                anc6_nutadvice: req.body.anc6_nutadvice,
                anc6_advicenausea: req.body.anc6_advicenausea,
                anc6_adviceheart: req.body.anc6_adviceheart,
                anc6_adviceconst: req.body.anc6_adviceconst,
                anc6_advicepedal: req.body.anc6_advicepedal,
                anc6_adviceleg: req.body.anc6_adviceleg,
                anc6_advicediabetic: req.body.anc6_advicediabetic,
                anc7_head: req.body.anc7_head,
                anc7_mict: req.body.anc7_mict,
                anc7_short: req.body.anc7_short,
                anc7_easy: req.body.anc7_easy,
                anc7_spot: req.body.anc7_spot,
                anc7_adequate: req.body.anc7_adequate,
                anc7_itching: req.body.anc7_itching,
                anc7_pog: req.body.anc7_pog,
                anc7_pallor: req.body.anc7_pallor,
                anc7_pedal: req.body.anc7_pedal,
                anc7_PR: req.body.anc7_PR,
                anc7_BP: req.body.anc7_BP,
                anc7_weight: req.body.anc7_weight,
                anc7_adviceothers: req.body.anc7_adviceothers,
                anc7_adviceTFe: req.body.anc7_adviceTFe,
                anc7_adviceman: req.body.anc7_adviceman,
                anc7_advicelabour: req.body.anc7_advicelabour,
                anc7_adviceDFMC: req.body.anc7_adviceDFMC,
                anc7_advicenausea: req.body.anc7_advicenausea,
                anc7_nutadvice: req.body.anc7_nutadvice,
                anc7_adviceheart: req.body.anc7_adviceheart,
                anc7_adviceheart: req.body.anc7_adviceconst,
                anc7_advicepedal: req.body.anc7_advicepedal,
                anc7_adviceleg: req.body.anc7_adviceleg,
                anc7_advicediabetic: req.body.anc7_advicediabetic,
                anc8_head: req.body.anc8_head,
                anc8_mict: req.body.anc8_mict,
                anc8_short: req.body.anc8_short,
                anc8_easy: req.body.anc8_easy,
                anc8_spot: req.body.anc8_spot,
                anc8_adequate: req.body.anc8_adequate,
                anc8_itching: req.body.anc8_itching,
                anc8_pog: req.body.anc8_pog,
                anc8_pallor: req.body.anc8_pallor,
                anc8_pedal: req.body.anc8_pedal,
                anc8_PR: req.body.anc8_PR,
                anc8_BP: req.body.anc8_BP,
                anc8_weight: req.body.anc8_weight,
                anc8_PA: req.body.anc8_PA,
                anc8_adviceothers: req.body.anc8_adviceothers,
                anc8_hisother: req.body.anc8_hisother,
                anc8_examothers: req.body.anc8_examothers,
                anc8_adviceTFe: req.body.anc8_adviceTFe,
                anc8_adviceman: req.body.anc8_adviceman,
                anc8_adviceDFMC: req.body.anc8_adviceDFMC,
                anc8_advicenausea: req.body.anc8_advicenausea,
                anc8_nutadvice: req.body.anc8_nutadvice,
                anc8_adviceheart: req.body.anc8_adviceheart,
                anc8_adviceconst: req.body.anc8_adviceconst,
                anc8_advicepedal: req.body.anc8_advicepedal,
                anc8_adviceleg: req.body.anc8_adviceleg,
                anc8_advicediabetic: req.body.anc8_advicediabetic
            }
        }
    ).then((result) => {
        res.status(200).json(result)
    }).catch((err) => { console.warn(err) })

})

router.get('/patientuhid_datalist', async (req, res) => {
    try {
        const patientdata = await Patientadddatabydoc.find({})
        res.send(patientdata)
    } catch (error) {
        res.sendStatus(error)
    }
})



router.put('/change_doc/:id', verifyToken, jsonParser, function (req, res) {
    jwt.verify(req.token, "secretkey", async (err, authData) => {
        if (err) {
            return res.sendStatus(403)
        } else {
            console.log("hello", req.params.id);
            User.update({ _id: req.params.id },
                {
                    $set: {
                        mobileDoctor: req.body.mobileDoctor,
                        docname: req.body.docname
                    }
                }
            ).then((result) => {
                res.status(200).json(result)
            }).catch((err) => { console.warn(err) })
        }
    })
})


router.get('/ancdatalist', async (req, res) => {
    try {
        const anc = await Anc.find({})
        res.send(anc)
    } catch (error) {
        res.sendStatus(error)
    }
})

function verifyToken(req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader != "undefined") {
        const bearer = bearerHeader.split(' ');
        const bearerToken = bearer[1]
        req.token = bearerToken;
        console.log(req.token)
        next();
    } else {
        res.sendStatus(403);
    }
}



module.exports = router