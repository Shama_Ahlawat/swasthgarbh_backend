const mongoose = require('mongoose');

const patientdataSchema = new mongoose.Schema({
    date:{type:String,required:true},
    sys_BP:{type:String,required:true},
    dys_BP:{type:String,required:true},
    headache:{type:Boolean},
    abdominal:{type:Boolean},
    visual:{type:Boolean},
    fetal_movement:{type:Boolean},
    swelling:{type:Boolean},
    urine:{type:String},
    bleed:{type:String},
    heart_rate:{type:String},
    bodyweight:{type:String,required:true},
    extracomments:{type:String},
    pat_mob:{type:String,required:true}
})

mongoose.model('Patientadddata',patientdataSchema);