const mongoose = require('mongoose');
const bcrypt = require('bcrypt')

const patient_regby_docSchema = new mongoose.Schema({
    mobile:{type:String,unique:true,required:true},
    password:{type:String,required:true},
    docmob:{type:String,required:true},
    docname:{type:String,required:true},
    name:{type:String,required:true},
    address:{type:String},
    email:{type:String},
    age:{type:String,required:true},
    high_bp_check:{type:Boolean},
    history_pre:{type:Boolean},
    mother_sis:{type:Boolean},
    obesity:{type:Boolean},
    baby:{type:Boolean},
    any_history:{type:Boolean},
    sestatus:{type:String,required:true},
    education:{type:String,required:true},
    lmpdate:{type:String,required:true},
    uhid:{type:String, required:true}
})


patient_regby_docSchema.pre('save', function(next){
    const patient_regby_doc = this;
    if(!patient_regby_doc.isModified('password')){
        return next()
    }
    bcrypt.genSalt(10,(err,salt)=>{
        if(err){
            return next(err)
        }
        bcrypt.hash(patient_regby_doc.password,salt,(err,hash)=>{
            if(err){
                return next(err)
            }
            patient_regby_doc.password = hash;
            next()
        })
       })
})

patient_regby_docSchema.methods.comparePassword = function (candidatePassword){
    const patient_regby_doc = this;
    return new Promise((resolve,reject)=>{
        bcrypt.compare(candidatePassword,patient_regby_doc.password,(err,isMatch)=>{
            if(err){
                return reject(err)
            }
            if (!isMatch){
                return reject(err)
            }
            resolve(true)
        })
    })

}

mongoose.model('Patient_regby_doc',patient_regby_docSchema);