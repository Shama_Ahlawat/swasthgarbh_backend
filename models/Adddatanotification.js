const mongoose = require('mongoose');

const adddatanotificationSchema = new mongoose.Schema({
    notification:{type:String},
    date:{type:String},
    pat_name:{type:String},
    pat_mobile:{type:String},
    doc_mobile:{type:String},
    doc_name:{type:String}
})

mongoose.model('Adddatanotification',adddatanotificationSchema);