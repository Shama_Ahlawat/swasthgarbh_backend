const mongoose = require('mongoose');

const ancSchema = new mongoose.Schema({
    anc1_date:{type:String},
    anc2_date:{type:String},
    anc3_date:{type:String},
    anc4_date:{type:String},
    anc5_date:{type:String},
    anc6_date:{type:String},
    anc7_date:{type:String},
    anc8_date:{type:String},
    anc1diabetes:{type:Boolean},
    anc1anaemia:{type:Boolean},
    anc1ultrasound:{type:Boolean},
    anc1HIV:{type:Boolean},
    anc1tetnus:{type:Boolean},
    anc1urinetest:{type:Boolean},
    anc2diabetes:{type:Boolean},
    anc2ultrasound:{type:Boolean},
    anc3diabetes:{type:Boolean},
    anc3anaemia:{type:Boolean},
    anc3urinetest:{type:Boolean},
    anc4diabetes:{type:Boolean},
    anc5diabetes:{type:Boolean},
    anc5urinetest:{type:Boolean},
    anc6diabetes:{type:Boolean},
    anc6anaemia:{type:Boolean},
    anc7diabetes:{type:Boolean},
    anc8diabetes:{type:Boolean},
    mobile:{type:String, required:true},
})

mongoose.model('Anc',ancSchema);