const mongoose = require('mongoose');
const bcrypt = require('bcrypt')

const doctorSchema = new mongoose.Schema({
    mobile:{type:String,unique:true,required:true},
    password:{type:String,required:true},
    name:{type:String,required:true},
    hospital:{type:String, required:true},
    email:{type:String, required:true},
    speciality:{type:String,required:true}
})

doctorSchema.pre('save', function(next){
    const doctor = this;
    if(!doctor.isModified('password')){
        return next()
    }
    bcrypt.genSalt(10,(err,salt)=>{
        if(err){
            return next(err)
        }
        bcrypt.hash(doctor.password,salt,(err,hash)=>{
            if(err){
                return next(err)
            }
            doctor.docpassword = hash;
            next()
        })
       })
})

doctorSchema.methods.comparePassword = function (candidatePassword){
    const doctor = this;
    return new Promise((resolve,reject)=>{
        bcrypt.compare(candidatePassword,doctor.password,(err,isMatch)=>{
            if(err){
                return reject(err)
            }
            if (!isMatch){
                return reject(err)
            }
            resolve(true)
        })
    })

}

mongoose.model('Doctor' ,doctorSchema);