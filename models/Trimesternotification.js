const mongoose = require('mongoose');

const trimesternotificationSchema = new mongoose.Schema({
    first_trim:{type:String},
    second_trim:{type:String},
    third_trim:{type:String},
    date:{type:String},
    doc_mobile:{type:String}
})

mongoose.model('Trimesternotification',trimesternotificationSchema);