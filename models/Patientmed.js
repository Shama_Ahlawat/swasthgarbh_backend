const mongoose = require('mongoose');

const patientmedSchema = new mongoose.Schema({
    mobile: { type: String, required: true },
    doc_mob: { type: String},
    sos_switch: { type: Boolean },
    checked: { type: String },
    med_name: { type: String, required: true },
    from_date: { type: String },
    to_date: { type: String },
    extra_med: { type: String },
    selected_days: [String],
    schedule_type: { type: String },
    med_reason: { type: String },
    med_quantity: { type: String },
    dose_count: { type: Number },
    alarm_timer:[String]
})

mongoose.model('Patientmed', patientmedSchema);