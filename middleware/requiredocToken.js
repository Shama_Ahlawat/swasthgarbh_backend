const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const Doctor = mongoose.model('Doctor');
const {jwtkey} = require('../keys')


module.exports = (req,res,next)=>{
    const{ authorization }=req.headers;
    if(!authorization){
        return res.status(401).send({error:"you must be logged in is it!!!"})
    }
    const token = authorization.replace("Bearer ","");
    console.log(token)

    // jwt.verify(jwtkey,async (err,payload)=>{
    jwt.verify(token,"secretkey",async (err,payload)=>{
        if (err){
            return res.status(401).send({error:"you must be logged in or it!!"})
        }
        const {doctorId} = payload;
        const doctor = await Doctor.findById(doctorId)
        req.doctor = doctor;
        console.log(doctor)
        console.log("match ho gya h")
        next();
    })

}


