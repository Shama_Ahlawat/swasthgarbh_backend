const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const Patient_regby_doc = mongoose.model('Patient_regby_doc');
const {jwtkey} = require('../keys')


module.exports = (req,res,next)=>{
    const{ authorization }=req.headers;
    if(!authorization){
        return res.status(401).send({error:"you must be logged in is it!!!"})
    }
    const token = authorization.replace("Bearer ","");
    console.log(token)

    // jwt.verify(jwtkey,async (err,payload)=>{
    jwt.verify(token,"secretkey",async (err,payload)=>{
        if (err){
            return res.status(401).send({error:"you must be logged in or it!!"})
        }
        const {patient_regby_docId} = payload;
        const patient_regby_doc = await Patient_regby_doc.findById(patient_regby_docId)
        req.patient_regby_doc = patient_regby_doc;
        console.log("hupp", patient_regby_doc)
        next();
    })
}


