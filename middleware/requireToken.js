const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const User = mongoose.model('User')
const {jwtkey} = require('../keys')


module.exports = (req,res,next)=>{
    const{ authorization }=req.headers;
    if(!authorization){
        return res.status(401).send({error:"you must be logged in is it!!!"})
    }
    const token = authorization.replace("Bearer ","");
    console.log(token)

    // jwt.verify(jwtkey,async (err,payload)=>{
    jwt.verify(token,"secretkey",async (err,payload)=>{
        if (err){
            return res.status(401).send({error:"you must be logged in or it!!"})
        }
        const {userId} = payload;
        const user = await User.findById(userId)
        req.user = user;
        console.log(user)
        next();
    })

}


