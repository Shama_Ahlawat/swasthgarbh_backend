const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const app = express()
const PORT = process.env.PORT || 7000
const { mongoUrl } = require('./keys')

require('./models/User');
require('./models/Doctor');
require('./models/Patientadddata');
require('./models/Patient_regby_doc');
require('./models/Patientmed');
require('./models/Anc');
require('./models/Patientadddatabydoc');
require('./models/Adddatanotification');
require('./models/Trimesternotification');

const requireToken = require('./middleware/requireToken')
const requiredocToken = require('./middleware/requiredocToken')
const requireregbydocToken = require('./middleware/requireregbydocToken')
const authRoutes = require('./routes/authRoutes')
app.use(bodyParser.json())
app.use(authRoutes)

mongoose.set('useCreateIndex', true);
mongoose.connect(mongoUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

mongoose.connection.on('connected', () => {
    console.log("yayyy!!!! connected to mogo")
})
mongoose.connection.on('error', (err) => {
    console.log("this is error", err)
})

app.get('/patient', requireToken, (req, res) => {
    console.log("patient api hit")
    res.send({
        name: req.user.name,
        address: req.user.address,
        age: req.user.age,
        mobile: req.user.mobile,
        email: req.user.email,
        high_bp_check: req.user.high_bp_check, history_pre: req.user.history_pre,
        mother_sis: req.user.mother_sis, obesity: req.user.obesity, baby: req.user.baby,
        any_history: req.user.any_history, sestatus: req.user.sestatus, education: req.user.education,
        lmpdate: req.user.lmpdate, mobileDoctor: req.user.mobileDoctor, docname: req.user.docname
    })
})

app.get('/doctor', requiredocToken, (req, res) => {
    res.send({
        name: req.doctor.name,
        speciality: req.doctor.speciality,
        hospital: req.doctor.hospital,
        mobile: req.doctor.mobile,
        email: req.doctor.email
    })
})

app.get('/doctorspatient', (req, res) => {
    res.send({
        name: req.patient_regby_doc.name,
        docmob: req.patient_regby_doc.docmob,
        docname: req.patient_regby_doc.docname,
        address: req.patient_regby_doc.address,
        password: req.patient_regby_doc.password,
        age: req.patient_regby_doc.age,
        mobile: req.patient_regby_doc.mobile,
        email: req.patient_regby_doc.email,
        high_bp_check: req.patient_regby_doc.high_bp_check, history_pre: req.patient_regby_doc.history_pre,
        mother_sis: req.patient_regby_doc.mother_sis, obesity: req.patient_regby_doc.obesity, baby: req.patient_regby_doc.baby,
        any_history: req.patient_regby_doc.any_history, sestatus: req.patient_regby_doc.sestatus, education: req.patient_regby_doc.education,
        lmpdate: req.patient_regby_doc.lmpdate, uhid: req.patient_regby_doc.uhid
    })

})


app.get('/patient_anc', requireToken, (req, res) => {
    res.send({
        name: req.patient_regby_doc.name,
        docmob: req.patient_regby_doc.docmob,
        docname: req.patient_regby_doc.docname,
        address: req.patient_regby_doc.address,
        password: req.patient_regby_doc.password,
        age: req.patient_regby_doc.age,
        mobile: req.patient_regby_doc.mobile,
        email: req.patient_regby_doc.email,
        high_bp_check: req.patient_regby_doc.high_bp_check, history_pre: req.patient_regby_doc.history_pre,
        mother_sis: req.patient_regby_doc.mother_sis, obesity: req.patient_regby_doc.obesity, baby: req.patient_regby_doc.baby,
        any_history: req.patient_regby_doc.any_history, sestatus: req.patient_regby_doc.sestatus, education: req.patient_regby_doc.education,
        lmpdate: req.patient_regby_doc.lmpdate, uhid: req.patient_regby_doc.uhid
    })
})

app.listen(PORT, () => {
    console.log("server running " + PORT)
})